from django.db import models

# Create your models here.
class Liste(models.Model):
    name = models.CharField(max_length=200) #in Performance ist Char gut, in der Praxis wird TextField genutzt, DJANGO spackt bei AdminTextfield rum
    
    def __str__(self): #sorgt dafür, dass beim Name der Liste auch wirklich der Name angezeigt wird
        return self.name
    
class Eintrag(models.Model):
    name = models.TextField()
    preis = models.FloatField(null=True, blank=True)
    menge = models.IntegerField(null=True, blank=True)
    geschaeft = models.ForeignKey(to="Geschaeft", on_delete=models.SET_NULL, null=True) #Wenn Geschäft gelöscht wird, Eintrag nicht mehr mit Geschäft verbunden
    liste = models.ForeignKey(to="Liste", on_delete=models.CASCADE) #Wenn Liste gelöscht wird, sind alle dazugehörigen Einträge weg
    status = models.BooleanField(default=False)
    
    def __str__(self):
        return self.name
    
class Geschaeft(models.Model):
    name = models.CharField(max_length=200)
    oeffnungszeiten = models.TextField(null=True) #null=True -> Eingabe ist optional
    
    def __str__(self):
        return self.name