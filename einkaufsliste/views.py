from django.shortcuts import render, redirect
from . import models
# Create your views here.

def index(request):
    listen = models.Liste.objects.all()
    geschaefte = models.Geschaeft.objects.all()
    context = {"listen": listen, "geschaefte": geschaefte}
    return render(request, "einkaufsliste/index.html", context)
    pass
    
def liste(request, liste_id):
    pass
    
def geschaeft(request, geschaeft_id):
    pass
    
def liste_anlegen(request): #kein extra Parameter, weil alle Parameter in POST drin sind
    models.Liste.objects.create(name=request.POST["liste_name"])
    return redirect("index")

def geschaeft_anlegen(request):
    models.Geschaeft.objects.create(name=request.POST["geschaeft_name"])
    return redirect("index")